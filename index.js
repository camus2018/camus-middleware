var yaml = require("js-yaml");
var fs   = require('fs');
var chalk = require("chalk");

var express = require('express');
var { ApolloServer, gql } = require('apollo-server-express');

var { makeExecutableSchema } =  require('graphql-tools');

var schemaBuilder = require('./schemaBuilder');
var resolvers = require('./resolver');

var Services = require("./Services"); // class instance
var Supervisor = require("./Supervisor");
var resources = require("./resources");
var utils = require("./utils/utils");
var logger = require("./utils/logger");
var config = require("./config/config");

var validators = require("./core/validators");

logger.info("Server starting ...");

resources.getResourcesSchema().then((resourcesSchema) => {

  logger.info("Resources schema successfully loaded");
  // Supervisor server
  var supervisor = new Supervisor(resourcesSchema);

  supervisor.app.listen(config.SUPERVISORPORT, () =>{
      logger.info(`🔍  Middleware Supervisor Now Running On localhost:${config.SUPERVISORPORT}/`);
  });

}).catch((error) => {

  logger.error("Failed to start server");
  logger.error(error);

})


// Get cdt, or throw exception on error

// GraphQL Middleware Setup

// resources.getResourcesSchema().then((rSchema) => {
//   // Create an express server and a GraphQL endpoint
//
//   logger.info(`Recieved schema from backend: ${Object.keys(rSchema).join(", ")}`);
//
//   const mappings = utils.loadYAML(config.MAPPINGSPATH)
//   const joins = utils.loadYAML(config.JOINSPATH)
//   logger.debug(JSON.stringify(mappings));
//
//   validators.validateCDT(cdt, rSchema).then((res) => logger.debug(res));
//   validators.validateMappings(cdt, mappings, rSchema).then((res) => res.forEach(e => logger.warn(e)));
//   validators.validateJoins(cdt, joins).then((res) => logger.warn(res))
//
//   const server = new ApolloServer({
//     typeDefs: schemaBuilder.formatTypes(rSchema),
//     resolvers: resolvers,
//     tracing: true
//   });
//
//   server.applyMiddleware({app});
//
//   app.listen(config.GRAPHQLPORT, () => logger.info(`🚀  Express GraphQL Server Now Running On localhost:${config.GRAPHQLPORT}/graphql`));
//
// }).catch((err) => {
//
//   logger.error("Error during server startup. Is back-end running? Is schema okey? ... ");
//   logger.error(`${err.message}`);
//
// });
