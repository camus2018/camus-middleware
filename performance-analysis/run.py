from graphqlclient import GraphQLClient
import json
import random
import statistics


client = GraphQLClient('http://localhost:8080/graphql')

json_data=open("../../dataset-generator/public/points.json").read()

data = json.loads(json_data)

print(len(data))

def getQuery(p):
    return  "{restaurant(query:\"" + \
            p["query"] + "\",lat: \"" + \
            str(p["latitude"]) + "\",lon:\"" + \
            str(p["longitude"]) + "\", limit:\"10\") {name address phone}}"

exectimes = []

for p in data: #random.sample(data, 5):
    print(getQuery(p))
    result = client.execute(getQuery(p))
    execs = json.loads(result)['extensions']['tracing']['duration']
    print(json.loads(result)['data'])
    exectimes.append(execs/10.0**9)

print(exectimes)

#
print('Sample Size: {}'.format(len(exectimes)))

print('Min: {} - Max: {}'.format(min(exectimes),max(exectimes)))
print('Average: {}'.format(statistics.mean(exectimes)))
print('Standard dev: {}'.format(statistics.pstdev(exectimes)))
print('Variance: {}'.format(statistics.pvariance(exectimes)))

# print(result)
