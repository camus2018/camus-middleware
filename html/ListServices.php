<?php
header('Content-Type: application/json');

$s = "string";

$response["restaurants"]["endpoint"] = "GETRestaurants.php";
$response["restaurants"]["params"] = ["city" => "string", "query" => "string", "lon" => "string", "lat" => "string"];
$response["restaurants"]["attributes"] = ["name" => $s,
                                          "address" => $s,
                                          "url" => $s,
                                          "latitude" => $s,
                                          "longitude" => $s,
                                          "geohash" => $s,
                                          "cuisines" => $s,
                                          "thumb" => $s,
                                          "phone" => $s,
                                          "rating" => $s
                                          ];

$response["events"]["endpoint"] = "GETEvents.php";
$response["events"]["params"] = ["city" => "string", "query" => "string", "lon" => "string", "lat" => "string"];
$response["events"]["attributes"] = ["title" => $s,
                                          "venue_name" => $s,
                                          "venue_address" => $s,
                                          "venue_url" => $s,
                                          "event_url" => $s,
                                          "all_day" => $s,
                                          "start_time" => $s,
                                          "end_time" => $s,
                                          "latitude" => $s,
                                          "longitude" => $s,
                                          "geohash" => $s
                                          ];

$response["news"]["endpoint"] = "GETNews.php";
$response["news"]["params"] = ["country" => "string", "language" => "string"];
$response["news"]["attributes"] = ["title" => $s,
                                          "description" => $s,
                                          "url" => $s,
                                          "image" => $s,
                                          "language" => $s,
                                          "date" => $s];

$response["weather"]["endpoint"] = "GETWeather.php";
$response["weather"]["params"] = ["lat" => $s, "lon" => $s];
$response["weather"]["attributes"] = ["summary" => $s,
                                          "temperature" => $s,
                                          "humidity" => $s,
                                          "windspeed" => $s,
                                          "pressure" => $s,
                                          "latitude" => $s,
					  "longitude" => $s];

echo json_encode($response);

?>
