<?php
include "./inc/dbinfo.inc";
include "./Geohash.php";
require './vendor/autoload.php';


Predis\Autoloader::register();

$conn = mysqli_connect(DB_SERVER, DB_USERNAME, DB_PASSWORD, DB_DATABASE);
$redis = new Predis\Client([
    'scheme' => 'tcp',
    'host'   => 'redis',
    'port'   => 6379,
]);
$g = new Geohash();
class Event {
    public $title, $venueName, $venueAddress, $venueURL, $eventURL, $allDay, $startTime, $endTime, $longitude, $latitude;

    function __construct($title = "", $venueName = "", $venueAddress = "", $venueURL = "", $eventURL = "", $allDay = "", $startTime = "", $endTime = "", $latitude = "", $longitude = "") {
        $this->title = $title;
        $this->venueName = $venueName;
        $this->venueAddress = $venueAddress;
        $this->venueURL = $venueURL;
        $this->eventURL = $eventURL;
        $this->allDay = $allDay;
        $this->startTime = $startTime;
        $this->endTime = $endTime;
        $this->latitude = $latitude;
        $this->longitude = $longitude;
    }
}

function flattenWithKeys(array $array, $childPrefix = '.', $root = '', $result = array()) {
	foreach($array as $k => $v) {
		if(is_array($v) || is_object($v))
		    $result = flattenWithKeys((array) $v, $childPrefix, $root . $k . $childPrefix, $result);
		else
		    $result[ $root . $k ] = $v;
	}
	return $result;
}

function sendRequest($url, $key, $useHeader) {
    if ($useHeader) {
        $opts = array(
          'http'=>array(
            'method'=>"GET",
            'header'=> $key,
          )
        );
    } else {
        $opts = array(
          'http'=>array(
            'method'=>"GET",
          )
        );
    }
    $context = stream_context_create($opts);
    $response = file_get_contents($url, false, $context);
    return json_decode($response, true);
}

function prepareResponse($response, $service) {
    $eventList = array();
    $resultArray = array();
    $responseJSON = $GLOBALS["servicesJSON"][$service];
    $responseDetails = $responseJSON["ResponseParams"];
    if (isset($responseDetails["loop"])) {
        foreach($response[$responseDetails["preloop"]] as $e) {
            $eventsList[] = $e[$responseDetails["postloop"]];
        }
    } else {
        $pathArray = explode("::", $responseDetails["arrayLocation"]);
        foreach ($pathArray as $p) {
            if (isset($response[$p]))
                $response = $response[$p];
        }
        $eventsList = $response;
    }
    if (count($eventsList) > 0) {
        foreach ($eventsList as $event) {
            $event = flattenWithKeys($event);
            $v = $responseDetails;
            if (isset($responseJSON["PostRequest"])) {
                $postDetails = $responseJSON["PostRequest"];
                if ($postDetails["scope"] == "item") {
                    $hasKey = isset($postDetails["key"]);
                    $postURL = $postDetails["url"] . $event[$postDetails["param"]]; //might need adaptation
                    $postObject = flattenWithKeys(sendRequest($postURL, isset($postDetails["key"]) ? $postDetails["key"] : "", $hasKey));
                } else {
                    //code here for list-level post request
                }
            }
            if (isset($responseJSON["CustomObject"])) {
                eval($responseJSON["CustomObject"]["constructor"]);
            } else {
                $eventObject = new Event(isset($v["title"], $event[$v["title"]]) ? $event[$v["title"]] : "",
                                         isset($v["venueName"], $event[$v["venueName"]]) ? $event[$v["venueName"]] : "",
                                         isset($v["venueAddress"], $event[$v["venueAddress"]]) ? $event[$v["venueAddress"]] : "",
                                         isset($v["venueURL"], $event[$v["venueURL"]]) ? $event[$v["venueURL"]] : "",
                                         isset($v["eventURL"], $event[$v["eventURL"]]) ? $event[$v["eventURL"]] : "",
                                         isset($v["allDay"], $event[$v["allDay"]]) ? $event[$v["allDay"]] : "",
                                         isset($v["startTime"], $event[$v["startTime"]]) ? $event[$v["startTime"]] : "",
                                         isset($v["endTime"], $event[$v["endTime"]]) ? $event[$v["endTime"]] : "",
                                         isset($v["longitude"], $event[$v["latitude"]]) ? $event[$v["latitude"]] : "",
                                         isset($v["latitude"], $event[$v["longitude"]]) ? $event[$v["longitude"]] : "");
            }
            $resultArray[] = $eventObject;
        }
        return $resultArray;
    }
}

$city = isset($_GET["city"]) ? $_GET["city"] : null;
$query = $_GET["query"] == null ? "" : $_GET["query"];
$lon = isset($_GET["lon"]) ? $_GET["lon"] : null;
$lat = isset($_GET["lat"]) ? $_GET["lat"] : null;
$response = array();
$found = false;

$sql = "Insert into EventsQueries (latitude, longitude, city, text) values ('$lat', '$lon', '$city', '$query')";
$conn->query($sql);

$queryHash = substr($g->encode($lat, $lon), 0, 5);
$time = date("Y-m-d H:i:s");
$key = "Events:::" . $queryHash . ":::" . $query;
$redis->set($key, $time);

if (isset($lon, $lat)) {
    $coords = $lat . "," . $lon;
}

$servicesAPI = scandir("./Events/", 1);
for ($i = 0; $i < count($servicesAPI) - 2; $i++) {
    $serviceName = substr($servicesAPI[$i], 0, strlen($servicesAPI[$i]) - 5);
    $servicesJSON[$serviceName] = json_decode(file_get_contents("./Events/" . $servicesAPI[$i]), true);
}

foreach($GLOBALS["servicesJSON"] as $name => $api) {
    $hasKey = isset($api["MainRequest"]["key"]);
    $mainDetails = $api["MainRequest"];
    if (isset($api["PreRequest"]) && !isset($coords)) {
        $preDetails = $api["PreRequest"];
        $preURL = $preDetails["url"] . $city;
        $preResponse = sendRequest($preURL, isset($preDetails["key"]) ? $preDetails["key"] : "", $hasKey);
        if (isset($preResponse)) {
            $preResponse = flattenWithKeys($preResponse);
            if (isset($preResponse[$preDetails["result"]])) {
                $city = $preResponse[$preDetails["result"]];
            } else {
                continue;
            }
        } else {
            continue;
        }
    } else {
        $city = isset($_GET["city"]) ? $_GET["city"] : null;
    }
    if (isset($mainDetails["coords"], $coords))
        $url = $mainDetails["url"] . $mainDetails["query"] . $query . $mainDetails["coords"] . $coords;
    else if (isset($mainDetails["geohash"], $coords))
        $url = $mainDetails["url"] . $mainDetails["query"] . $query . $mainDetails["geohash"] . $g->encode($lat, $lon);
    else if (isset($mainDetails["lon"], $mainDetails["lat"], $coords))
        $url = $mainDetails["url"] . $mainDetails["query"] . $query . $mainDetails["lon"] . $lon . $mainDetails["lat"] . $lat;
    else if (isset($mainDetails["city"], $city))
        $url = $mainDetails["url"] . $mainDetails["query"] . $query . $mainDetails["city"] . $city;
    else
        continue;
    $serviceResponse = sendRequest($url , isset($mainDetails["key"]) ? $mainDetails["key"] : "", $hasKey);
    if (isset($serviceResponse)) {
        $result = prepareResponse($serviceResponse, $name);
        $t[$name] = $result;
	if ($result) {
            $found = true;
            foreach ($result as $r) {
                foreach ($r as $key => $att) {
                    $r->$key = $conn->real_escape_string($att);
                }
                $sql = "Insert into Events (source, query_lat, query_long, query_city, query_text, title, venue_name, venue_address, venue_url, event_url, all_day, start_time, end_time, latitude, longitude, geohash, created_at) values ('$name', $lat, $lon, '$city', '$query', '$r->title', '$r->venueName', '$r->venueAddress', '$r->venueURL', '$r->eventURL', '$r->allDay', '$r->startTime', '$r->endTime', " . floatval($r->latitude) . ", " . floatval($r->longitude) . ", '" . $g->encode($r->latitude, $r->longitude) . "', NOW())"
                . " On Duplicate Key Update query_text = IF (query_text NOT LIKE '%$query%', CONCAT(query_text, ',$query'), query_text), venue_address = '$r->venueAddress', venue_url = '$r->venueURL', event_url = '$r->eventURL', all_day = '$r->allDay', start_time = '$r->startTime', end_time = '$r->endTime', updated_at = NOW()";
		if ($conn->query($sql) === FALSE) {
                     // echo "Error: " . $sql . "<br>" . $conn->error;
                    continue;
                }

            }
	    $output[$name] = $result;
        }
    }
}

if ($found)
    echo json_encode(array("code" => 200, "message" => "Database Ready"));
else
    echo json_encode(array("code" => 404, "message" => "No Results Found"));

?>
