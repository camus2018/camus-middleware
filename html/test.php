<?php
include "./inc/dbinfo.inc";
include "./Geohash.php";
require 'Predis/Autoloader.php';


Predis\Autoloader::register();

$connection = mysqli_connect(DB_SERVER, DB_USERNAME, DB_PASSWORD);
$redis = new Predis\Client();

$city = isset($_GET["city"]) ? $_GET["city"] : null;
$query = $_GET["query"];
$lon = isset($_GET["lon"]) ? $_GET["lon"] : null;
$lat = isset($_GET["lat"]) ? $_GET["lat"] : null;
$g = new Geohash();
$enc = substr($g->encode($lat, $lon), 0, 5);
$time = date("Y-m-d H:i:s");
$key = $enc . ":::" . $query;
$redis->set($key, $time);


?>
