<?php
include "./inc/dbinfo.inc";
include "./Geohash.php";
require 'Predis/Autoloader.php';


Predis\Autoloader::register();

$connection = mysqli_connect(DB_SERVER, DB_USERNAME, DB_PASSWORD);
$redis = new Predis\Client();

$list = $redis->keys("*");
asort($list);
foreach ($list as $key)
{
	$value = $redis->get($key);
	$ans[$key] = $value;
}
asort($ans);
foreach ($ans as $key => $value) {
	echo "<b>Key:</b> $key <br /><b>Value:</b> $value <br /><br />";
}
?>
