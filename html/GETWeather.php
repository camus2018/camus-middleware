<?php
header('Content-Type: application/json');

include "./Geohash.php";

$g = new Geohash();

class Weather { 
    public $summary, $temperature, $humidity, $windspeed, $pressure, $latitude, $longitude;
    
    function __construct($summary = "", $temperature = "", $humidity = "", $windspeed = "", $pressure = "", $latitude = "", $longitude = "") {
        $this->summary = $summary;
        $this->temperature = $temperature;
        $this->humidity = $humidity;
        $this->windspeed = $windspeed;
        $this->pressure = $pressure;
        $this->latitude = $latitude;
        $this->longitude = $longitude;
    }
}  

function flattenWithKeys(array $array, $childPrefix = '.', $root = '', $result = array()) {
	foreach($array as $k => $v) {
		if(is_array($v) || is_object($v)) 
		    $result = flattenWithKeys((array) $v, $childPrefix, $root . $k . $childPrefix, $result);
		else 
		    $result[ $root . $k ] = $v;
	}
	return $result;
}


/********** Generic SendRequest Function **************/
function sendRequest($url, $key, $useHeader) {
    if ($useHeader) {
        $opts = array(
          'http'=>array(
            'method'=>"GET",
            'header'=> $key,
          )
        );
    } else {
        $opts = array(
          'http'=>array(
            'method'=>"GET",
          )
        );
    }
    $context = stream_context_create($opts);
    $response = file_get_contents($url, false, $context);
    return json_decode($response, true);
}
/********** Generic SendRequest Function **************/

/********** Generic PrepareResponse Function **************/
function prepareResponse($response, $service) {
    $responseJSON = $GLOBALS["servicesJSON"][$service];
    $v = $responseJSON["ResponseParams"];
    $weather = flattenWithKeys($response);
    $weatherObject = new Weather(isset($v["summary"], $weather[$v["summary"]]) ? $weather[$v["summary"]] : "", 
                             isset($v["temperature"], $weather[$v["temperature"]]) ? $weather[$v["temperature"]] : "",
                             isset($v["humidity"], $weather[$v["humidity"]]) ? $weather[$v["humidity"]] : "", 
                             isset($v["windspeed"], $weather[$v["windspeed"]]) ? $weather[$v["windspeed"]] : "",
                             isset($v["pressure"], $weather[$v["pressure"]]) ? $weather[$v["pressure"]] : "", 
                             isset($v["longitude"], $weather[$v["latitude"]]) ? $weather[$v["latitude"]] : "",
                             isset($v["latitude"], $weather[$v["longitude"]]) ? $weather[$v["longitude"]] : "");
    return $weatherObject;
}
/********** Generic PrepareResponse Function **************/

$lon = isset($_GET["lon"]) ? $_GET["lon"] : null;
$lat = isset($_GET["lat"]) ? $_GET["lat"] : null;
$response = array();
$found = false;

if (isset($lon, $lat)) {
    $coords = $lat . "," . $lon;
}

$servicesAPI = scandir("./Weather/", 1);
for ($i = 0; $i < count($servicesAPI) - 2; $i++) {
    $serviceName = substr($servicesAPI[$i], 0, strlen($servicesAPI[$i]) - 5);
    $servicesJSON[$serviceName] = json_decode(file_get_contents("./Weather/" . $servicesAPI[$i]), true);
}
foreach($GLOBALS["servicesJSON"] as $name => $api) {
    $mainDetails = $api["MainRequest"];
    if (isset($mainDetails["coords"], $coords))
        $url = $mainDetails["url"] . $mainDetails["coords"] . $coords;
    else if (isset($mainDetails["lon"], $mainDetails["lat"], $coords))
        $url = $mainDetails["url"] . $mainDetails["lon"] . $lon . $mainDetails["lat"] . $lat;
    else
        continue;
    if (isset($mainDetails["params"])) {
        $url .= $mainDetails["params"];
    }
    $serviceResponse = sendRequest($url , "", false);
    if (isset($serviceResponse)) {
        $result = prepareResponse($serviceResponse, $name);
        $response[] = $result;
    }
}

echo json_encode($response);
