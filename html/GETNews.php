<?php
header('Content-Type: application/json');

class News { 
    public $title, $description, $url, $image, $language, $date;
    
    function __construct($title = "", $description = "", $url = "", $image = "", $language = "", $date = "") {
        $this->title = $title;
        $this->description = $description;
        $this->url = $url;
        $this->imge = $image;
        $this->language = $language;
        $this->date = $date;
    }
} 

function flattenWithKeys(array $array, $childPrefix = '.', $root = '', $result = array()) {
	foreach($array as $k => $v) {
		if(is_array($v) || is_object($v)) 
		    $result = flattenWithKeys((array) $v, $childPrefix, $root . $k . $childPrefix, $result);
		else 
		    $result[ $root . $k ] = $v;
	}
	return $result;
}

/********** Generic SendRequest Function **************/
function sendRequest($url, $key, $useHeader) {
    if ($useHeader) {
        $opts = array(
          'http'=>array(
            'method'=>"GET",
            'header'=> $key,
          )
        );
    } else {
        $opts = array(
          'http'=>array(
            'method'=>"GET",
          )
        );
    }
    $context = stream_context_create($opts);
    $response = @file_get_contents($url, false, $context);
    return json_decode($response, true);
}
/********** Generic SendRequest Function **************/

/********* Generic PrepareResponse Function ************/
function prepareResponse($response, $service) {
    $newsList = array();
    $resultArray = array();
    $responseJSON = $GLOBALS["servicesJSON"][$service];
    $responseDetails = $responseJSON["ResponseParams"];
    if (isset($responseDetails["loop"])) {
        foreach($response[$responseDetails["preloop"]] as $r) {
            $newsList[] = $r[$responseDetails["postloop"]];
        }
    } else {
        $pathArray = explode("::", $responseDetails["arrayLocation"]);
        foreach ($pathArray as $p) {
            if (isset($response[$p]))
                $response = $response[$p];
        }
        $newsList = $response;
    }
    if (count($newsList) > 0) {
        foreach ($newsList as $news) {
            $news = flattenWithKeys($news);
            $v = $responseDetails;
            if (isset($responseJSON["PostRequest"])) {
                $postDetails = $responseJSON["PostRequest"];
                if ($postDetails["scope"] == "item") {
                    $hasKey = isset($postDetails["key"]);
                    $postURL = $postDetails["url"] . $event[$postDetails["param"]]; //might need adaptation
                    $postObject = flattenWithKeys(sendRequest($postURL, $postDetails["key"] ?? "", $hasKey));
                } else {
                    //code here for list-level post request
                }
            }
            if (isset($responseJSON["CustomObject"])) {
                eval($responseJSON["CustomObject"]["constructor"]);
            } else {
                $newsObject = new News(isset($v["title"], $news[$v["title"]]) ? $news[$v["title"]] : "", 
                                                   isset($v["description"], $news[$v["description"]]) ? $news[$v["description"]] : "", 
                                                   isset($v["url"], $news[$v["url"]]) ? $news[$v["url"]] : "", 
                                                   isset($v["image"], $news[$v["image"]]) ? $news[$v["image"]] : "",
                                                   isset($v["language"], $news[$v["language"]]) ? $news[$v["language"]] : "", 
                                                   isset($v["date"], $news[$v["date"]]) ? $news[$v["date"]] : "");
            }
           $resultArray[] = $newsObject;
        }
        return $resultArray;
    }
}
/********* Generic PrepareResponse Function ************/

$country = isset($_GET["country"]) ? $_GET["country"] : null;
$language = isset($_GET["language"]) ? $_GET["language"] : null;
$response = array();

$servicesAPI = scandir("./News/");
for ($i = 2; $i < count($servicesAPI); $i++) {
    $serviceName = substr($servicesAPI[$i], 0, strlen($servicesAPI[$i]) - 5);
    $servicesJSON[$serviceName] = json_decode(file_get_contents("./News/" . $servicesAPI[$i]), true);
}

foreach($GLOBALS["servicesJSON"] as $name => $api) {
    $hasKey = isset($api["MainRequest"]["key"]);
    $mainDetails = $api["MainRequest"];
    if (isset($api["PreRequest"])) {
        $preDetails = $api["PreRequest"];
        $preURL = $preDetails["url"] . $city;
        $preResponse = sendRequest($preURL, $preDetails["key"] ?? "", $hasKey);
        if (isset($preResponse)) {
            $preResponse = flattenWithKeys($preResponse);
            if (isset($preResponse[$preDetails["result"]])) {
                $city = $preResponse[$preDetails["result"]];
            } else {
                continue;
            }
        } else {
            continue;
        }
    }
    $url = $mainDetails["url"] . $mainDetails["country"] . $country;
    if (isset($mainDetails["language"]))
        $url .= $mainDetails["language"] . $language;
    $serviceResponse = sendRequest($url , $mainDetails["key"] ?? "", $hasKey);
    if (isset($serviceResponse)) {
        $result = prepareResponse($serviceResponse, $name);
        if ($result != null)
            $response = array_merge($response, $result);
    }
}
echo json_encode($response);

?>
