<?php
header('Content-Type: application/json');

include "./Geohash.php";
include "./inc/dbinfo.inc";
require './vendor/autoload.php';

Predis\Autoloader::register();
$conn = mysqli_connect(DB_SERVER, DB_USERNAME, DB_PASSWORD, DB_DATABASE);
$redis = new Predis\Client([
    'scheme' => 'tcp',
    'host'   => 'redis',
    'port'   => 6379,
]);
$g = new Geohash();

class Restaurant {
    public $name, $address, $url, $latitude, $longitude, $cuisines, $thumb, $phone, $rating;

    function __construct($name = "", $address = "", $url = "", $latitude = "", $longitude = "", $cuisines = "", $thumb = "", $phone = "", $rating = "") {
        $this->name = $name;
        $this->address = $address;
        $this->url = $url;
        $this->latitude = $latitude;
        $this->longitude = $longitude;
        $this->cuisines = $cuisines;
        $this->thumb = $thumb;
        $this->phone = $phone;
        $this->rating = $rating;
    }
}

function flattenWithKeys(array $array, $childPrefix = '.', $root = '', $result = array()) {
	foreach($array as $k => $v) {
		if(is_array($v) || is_object($v))
		    $result = flattenWithKeys((array) $v, $childPrefix, $root . $k . $childPrefix, $result);
		else
		    $result[ $root . $k ] = $v;
	}
	return $result;
}

/********** Generic SendRequest Function **************/
function sendRequest($url, $key, $useHeader) {
    if ($useHeader) {
        $opts = array(
          'http'=>array(
            'method'=>"GET",
            'header'=> $key,
          )
        );
    } else {
        $opts = array(
          'http'=>array(
            'method'=>"GET",
          )
        );
    }
    $context = stream_context_create($opts);
    $response = @file_get_contents($url, false, $context);
    return json_decode($response, true);
}
/********** Generic SendRequest Function **************/

/********* Generic PrepareResponse Function ************/
function prepareResponse($response, $service) {
    $restaurantList = array();
    $resultArray = array();
    $responseJSON = $GLOBALS["servicesJSON"][$service];
    $responseDetails = $responseJSON["ResponseParams"];
    if (isset($responseDetails["loop"])) {
        foreach($response[$responseDetails["preloop"]] as $r) {
            $restaurantList[] = $r[$responseDetails["postloop"]];
        }
    } else {
        $pathArray = explode("::", $responseDetails["arrayLocation"]);
        foreach ($pathArray as $p) {
            if (isset($response[$p]))
                $response = $response[$p];
        }
        $restaurantList = $response;
    }
    if (count($restaurantList) > 0) {
        foreach ($restaurantList as $restaurant) {
            $restaurant = flattenWithKeys($restaurant);
            $v = $responseDetails;
            if (isset($responseJSON["PostRequest"])) {
                $postDetails = $responseJSON["PostRequest"];
                if ($postDetails["scope"] == "item") {
                    $hasKey = isset($postDetails["key"]);
                    $postURL = $postDetails["url"] . $event[$postDetails["param"]]; //might need adaptation
                    $postObject = flattenWithKeys(sendRequest($postURL, isset($postDetails["key"]) ? $postDetails["key"] : "", $hasKey));
                } else {
                    //code here for list-level post request
                }
            }
            if (isset($responseJSON["CustomObject"])) {
                eval($responseJSON["CustomObject"]["constructor"]);
            } else {
                $restaurantObject = new Restaurant(isset($v["name"], $restaurant[$v["name"]]) ? $restaurant[$v["name"]] : "",
                                                   isset($v["address"], $restaurant[$v["address"]]) ? $restaurant[$v["address"]] : "",
                                                   isset($v["url"], $restaurant[$v["url"]]) ? $restaurant[$v["url"]] : "",
                                                   isset($v["latitude"], $restaurant[$v["latitude"]]) ? $restaurant[$v["latitude"]] : "",
                                                   isset($v["longitude"], $restaurant[$v["longitude"]]) ? $restaurant[$v["longitude"]] : "",
                                                   isset($v["cuisines"], $restaurant[$v["cuisines"]]) ? $restaurant[$v["cuisines"]] : "",
                                                   isset($v["thumb"], $restaurant[$v["thumb"]]) ? $restaurant[$v["thumb"]] : "",
                                                   isset($v["phone"], $restaurant[$v["phone"]]) ? $restaurant[$v["phone"]] : "",
                                                   isset($v["rating"], $restaurant[$v["rating"]]) ? $restaurant[$v["rating"]] : "");
            }
           $resultArray[] = $restaurantObject;
        }
        return $resultArray;
    }
}
/********* Generic PrepareResponse Function ************/

$city = isset($_GET["city"]) ? $_GET["city"] : null;
$query = $_GET["query"] == null ? "" : $_GET["query"];
$lon = isset($_GET["lon"]) ? $_GET["lon"] : null;
$lat = isset($_GET["lat"]) ? $_GET["lat"] : null;
$found = false;
$response = array();

$sql = "Insert into RestaurantsQueries (latitude, longitude, city, text, created_at) values ('$lat', '$lon', '$city', '$query', NOW())";
$conn->query($sql);

$queryHash = substr($g->encode($lat, $lon), 0, 5);
$time = date("Y-m-d H:i:s");
$key = "Restaurants:::" . $queryHash . ":::" . $query;
$redis->set($key, $time);

if (isset($lon, $lat)) {
    $coords = $lat . "," . $lon;
}

$servicesAPI = scandir("./Restaurants/", 1);
for ($i = 0; $i < count($servicesAPI) - 2; $i++) {
    $serviceName = substr($servicesAPI[$i], 0, strlen($servicesAPI[$i]) - 5);
    $servicesJSON[$serviceName] = json_decode(file_get_contents("./Restaurants/" . $servicesAPI[$i]), true);
}

foreach($GLOBALS["servicesJSON"] as $name => $api) {
    $hasKey = isset($api["MainRequest"]["key"]);
    $mainDetails = $api["MainRequest"];
    if (isset($api["PreRequest"]) && !isset($coords)) {
        $preDetails = $api["PreRequest"];
        $preURL = $preDetails["url"] . $city;
        $preResponse = sendRequest($preURL, isset($preDetails["key"]) ? $preDetails["key"] : "", $hasKey);
        if (isset($preResponse)) {
            $preResponse = flattenWithKeys($preResponse);
            if (isset($preResponse[$preDetails["result"]])) {
                $city = $preResponse[$preDetails["result"]];
            } else {
                continue;
            }
        } else {
            continue;
        }
    } else {
        $city = isset($_GET["city"]) ? $_GET["city"] : null;
    }
    if (isset($mainDetails["coords"], $coords))
        $url = $mainDetails["url"] . $mainDetails["query"] . $query . $mainDetails["coords"] . $coords;
    else if (isset($mainDetails["geohash"], $coords))
        $url = $mainDetails["url"] . $mainDetails["query"] . $query . $mainDetails["geohash"] . $g->encode($lat, $lon);
    else if (isset($mainDetails["lon"], $mainDetails["lat"], $coords))
        $url = $mainDetails["url"] . $mainDetails["query"] . $query . $mainDetails["lon"] . $lon . $mainDetails["lat"] . $lat;
    else
        $url = $mainDetails["url"] . $mainDetails["query"] . $query . $mainDetails["city"] . $city;
    $serviceResponse = sendRequest($url , isset($mainDetails["key"]) ? $mainDetails["key"] : "", $hasKey);
    if (isset($serviceResponse)) {

        $result = prepareResponse($serviceResponse, $name);
        if ($result) {
            $found = true;
                foreach ($result as $r) {
                foreach ($r as $key => $att) {
                    $r->$key = $conn->real_escape_string($att);
                }
                $sql = "Insert into Restaurants (source, query_lat, query_long, query_city, query_text, name, address, url, latitude, longitude, geohash, cuisines, thumb, phone, rating, created_at) values ('$name', $lat, $lon, '$city', '$query', '$r->name', '$r->address', '$r->url', " . floatval($r->latitude) . ", " . floatval($r->longitude) . ", '" . $g->encode($r->latitude, $r->longitude) . "', '$r->cuisines', '$r->thumb', '$r->phone', " . floatval($r->rating) . ", NOW())"
                . " On Duplicate Key Update query_text = IF (query_text NOT LIKE '%$query%', CONCAT(query_text, ',$query'), query_text), url = '$r->url', cuisines = '$r->cuisines', thumb = '$r->thumb', phone = '$r->phone', rating = " . floatval($r->rating) . ", updated_at = NOW()";
                if ($conn->query($sql) === FALSE) {
                    echo "Error: " . $sql . "<br>" . $conn->error;
                    continue;
                }
            }
        }
    }
}
if ($found)
    echo json_encode(array("code" => 200, "message" => "Database Ready"));
else
    echo json_encode(array("code" => 404, "message" => "No Results Found"));

?>
