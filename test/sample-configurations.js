const cdt = {
            	"cdt": {
            		  "interest_topics": [
            		    "restaurants",
            		    "events",
            		    "weather"
            		  ],
            		  "user_type": [
            		    "young",
            		    "family_with_children",
            		    "adult"
            		  ],
            		  "location": [
            		    "lat",
            		    "long"
            		  ]
            		}
            }

const mapping = {
        	"user_type": {
        		"young": {
        			"restaurants": {
        				"query": "burger",
        				"lat": "CDT.lat",
        				"lon": "CDT.lon",
        				"limit": "CDT.limit"
        			}
        		}
        	}
}
