var chai = require('chai')
var expect = chai.expect
var chaiHttp = require('chai-http')
var chaiGraphQL = require('chai-http')
var fs = require('fs')

var config = require('../config/config');
var utils = require('../utils/utils');

const GQLURL = `http://localhost:${config.GRAPHQLPORT}`
const SUPURL = `http://localhost:${config.SUPERVISORPORT}`

chai.use(chaiHttp);
chai.use(chaiGraphQL);

const CDTSAMPLE = {
  interest_topics: ["restaurants", "events"],
  user_type: ["adult", "young"]
}


const MAPPINGSAMPLE = {
  "any": {
    "restaurants": {
      "lat": "CDT.lat",
      "lon": "CDT.lon"
    },
    "events": {
      "lat": "CDT.lat",
      "lon": "CDT.lon"
    }
  },
  "user_type": {
    "young": {
      "restaurants": {
        "query": "burger"
      },
      "events": {
        "query": "music"
      }
    }
  }
}


describe('Supervising operations workflow', () => {
  // The tests have to be performed in sequence (not async)
  // because they depend on each other
  before(() => {

      if (fs.existsSync(config.MAPPINGSPATH)) fs.unlinkSync(config.MAPPINGSPATH);
      if (fs.existsSync(config.CDTPATH)) fs.unlinkSync(config.CDTPATH);
      if (fs.existsSync(config.JOINSPATH)) fs.unlinkSync(config.JOINSPATH);

  });

  it("Submit new configuration", (done) => {
      chai.request(SUPURL)
          .post('/submit')
          .send({cdt: CDTSAMPLE, mappings: MAPPINGSAMPLE})
          .then(res => {
              return chai.request(SUPURL).get('/cdt')
          })
          .then(res => {
              const newCDT = JSON.parse(res.text)
              expect(newCDT).to.deep.equal(CDTSAMPLE);
              console.log('Submitted new CDT');
              return newCDT;
          })
          .then(newCDT => {
              const storedCDT = utils.loadYAML(config.CDTPATH);
              expect(storedCDT).to.deep.equal(newCDT);
              done()
          }).catch(e => done(e));
  });


  // it("GET /cdt", () => {
  //     return chai.request(SUPURL)
  //         .get('/cdt')
  //         .then(res => {
  //               expect(JSON.parse(res.text)).to.deep.equal(utils.loadYAML(config.CDTPATH))
  //         });
  // });
  //
  // it("POST /submit: check if cdt is saved", () => {
  //     return chai.request(SUPURL)
  //                .post('/submit')
  //                .send({cdt: CDTSAMPLE})
  //                .then(res => {
  //                   const newCDT = JSON.parse(res.text)
  //                   expect(newCDT.result[0].cdt).to.deep.equal(utils.loadYAML(config.CDTPATH))
  //                });
  // });
  //
  // it("GET /reload: ", () => {
  //     return chai.request(SUPURL)
  //                .get('/reload')
  //                .then(res => console.log(res.text))
  // });

});
