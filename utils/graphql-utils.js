var _ = require("lodash");

var logger = require("./logger");

function getArgsFromInfo(info) {

  var args = {};

  if(!_.has(info, 'operation.selectionSet.selections')) {
      logger.error("ERROR ===== info object is not what was expected");
  } else { 
      info.operation.selectionSet.selections.map((s) => {
        s.arguments.map((a) => {
          args[a.name.value] = a.value.value;
        });
    });
  }

  return args;
}

module.exports = {
  getArgsFromInfo: getArgsFromInfo
}
