const {createLogger, format, transports} = require('winston');
const { combine, timestamp, label, printf } = format;

const logformat = printf(info => {
  return `${info.timestamp} ${info.level.toUpperCase()}: ${info.message}`;
});

const logger = createLogger({
  level: 'debug',
  format: combine(
    timestamp(),
    logformat
  ),
  transports: [
    new transports.Console()
  ]
})

module.exports = logger;
