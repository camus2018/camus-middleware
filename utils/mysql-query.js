var mysql = require("mysql");
var _     = require("lodash");

var utils = require("./utils");
var logger = require("./logger");

const LAT = "lat";
const LON = "lon";
const RADIUS = "radius";
const LIMIT = "limit";
const WHERE = " WHERE ";
const AND = " AND ";
const QUERY = "query";
const QUERYDB = "query_text";
const CREATEDATDB = "created_at";
const LIMITSQL = " LIMIT ";

const defaultRadius = 5;

const query = "SELECT * FROM ?? ";
const geoCondS = "DISTANCE_BETWEEN(?, ?, latitude, longitude) < ? "
const geoCond = geoCondS
                + "ORDER BY DISTANCE_BETWEEN(?, ?, latitude, longitude) ASC";

const condition = "?? = ?";

function prepareQuery(endPoint, params) {
  // should add params

  var q = mysql.format(query, [utils.getTableName(endPoint)]);
  var condList = [];

  // logger.debug(params);
  // IF params have a query text
  if (_.has(params, QUERY)) {
    condList.push(mysql.format(condition, [QUERYDB, params[QUERY]]));
  }

  // If parameters have GPS location
  if (_.has(params, LAT) && _.has(params, LON)) {

    var latNum = parseFloat(params[LAT]);
    var lonNum = parseFloat(params[LON]);

    var radius = _.has(params, RADIUS) ? params.radius : defaultRadius;
    // This should be last as it has Order by
    condList.push(mysql.format(geoCond, [latNum, lonNum, defaultRadius, latNum, lonNum]));

  }

  // If there are conditions add them
  if(condList.length) {
    q = q + WHERE + condList.join(AND);
  }

  // If query has a limit
  //if(_.has(params, LIMIT)) {
    q = q + LIMITSQL + '5' //params.limit;
  //}

  logger.debug(`SQL ENTITIES: ${q}`);

  return q;

}

function prepareCacheQuery(endPoint, params) {

  var q = mysql.format(query, [utils.getCacheTable(endPoint)]);
  var condList = [];

  var latNum = parseFloat(params[LAT]);
  var lonNum = parseFloat(params[LON]);

  condList.push(mysql.format(geoCondS, [latNum, lonNum, 1]));
  condList.push(CREATEDATDB + ' >=  NOW() - INTERVAL 1 MONTH');
  condList.push(`levenshtein_ratio(\'${params[QUERY]}\', Text) >= 50`);

  // Exact match query
  //condList.push(mysql.format(condition, ['Text', params[QUERY]]))

  q = q + WHERE + condList.join(AND);

  logger.debug(`SQL CACHE: ${q}`)

  return q;

}

module.exports = {
  prepareQuery: prepareQuery,
  prepareCacheQuery: prepareCacheQuery
}
