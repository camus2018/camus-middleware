var mysql = require("promise-mysql");

var dbConfig = require("../config/config").db;
var mysqlQuery = require("./mysql-query");
var utils = require("./utils");
var logger = require("./logger");

var pool = mysql.createPool(dbConfig);

function executeQuery(endPoint, params) {
  return pool.query(mysqlQuery.prepareQuery(endPoint, params)).then( (res)=> {
    // logger.debug(res.length);
    return res;
  });
}

function rawQuery(query) {
  logger.warn("RAW QUERY EXECUTION !")
  return pool.query(query).then( (res)=> {
    // logger.debug(res.length);
    return res;
  });
}

function checkCache(endPoint, params) {
  logger.debug(`Checking cache ${utils.getCacheTable(endPoint)}`)
  return pool.query(mysqlQuery.prepareCacheQuery(endPoint, params)).then((res)=> {
      return res.length;
  });
}

module.exports = {
  rawQuery: rawQuery,
  executeQuery: executeQuery,
  checkCache: checkCache

}
