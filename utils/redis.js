var {redisHost} = require("../config/config")
const {promisify} = require('util');

var utils = require("./utils");
var logger = require("./logger");


var geohash = require("ngeohash");
var stringSimilarity = require("string-similarity");


var redis = require("redis"),
    client = redis.createClient({host: redisHost});

client.on("error", function (err) {
    logger.error("Error " + err);
});

client.on('connect', function() {
    logger.info('Redis client connected');
});


const getAsync = promisify(client.get).bind(client);
const setAsync = promisify(client.set).bind(client);
const getKeys = promisify(client.keys).bind(client);
const scan = promisify(client.scan).bind(client);

async function scanAsync(cursor, pattern, results) {
   return scan(cursor, 'MATCH', pattern, 'COUNT', '10')
       .then(function(reply) {
           let keys = reply[1]
           keys.forEach(function(key) {
               // console.log(key);
               results.push(key)
           })
           cursor = reply[0]
           if(cursor === '0') {
                // console.log('Scan complete')
           } else {
               return scanAsync(cursor, pattern, results)
           }
       })
}

const matchKey = async function(pattern) {
  results = []
  await scanAsync('0', pattern, results);
  return results;
}

function checkRedisCache(query, endpoint, lat, long) {
  var g = geohash.encode(lat,long, 5);

  return matchKey(utils.getTableName(endpoint)+":::"+g+":::*")
  .then((matches) => {
    var promises = []
    var queries = []

    // console.log(query)
    // console.log(endpoint)
    // console.log(`${lat} ${long}`)

    matches.forEach((m) => {
      mq = m.split(":::")[2];
      if(query == mq){
        queries.push({key: m, similarity: 1})
      } else {
        var s = stringSimilarity.compareTwoStrings(query, mq)
        if (s >= 0.5)
          queries.push({key: m, similarity: s})
      }
    });
    // console.log(queries)
    queries.forEach((q) => {
      promises.push(getAsync(q.key).then((ts) => {
        var validity = (Date.now() - (new Date(ts)))/(3600*1000*24) < 30
        return validity ? q : null;
      }));
    });

    return Promise.all(promises).then((listq) => {
      var max = 0;
      var mostSim = null;
      listq.forEach((q) => {
        if (q.similarity > max)
          max = q.similarity;
          mostSim = q.key.split(":::")[2];
      })
      return mostSim;
    })

    // return Promise.all(promises).then((tss) => {
    //   // console.log(tss);
    //   var flag = false;
    //   tss.forEach((t) => {
    //     // console.log((Date.now() - (new Date(t)))/(3600*1000*24))
    //     if((Date.now() - (new Date(t)))/(3600*1000*24) < 30) {
    //       flag = true;
    //     }
    //   });
    //   return flag;
    // });
  })
}


module.exports = {
  get: getAsync,
  set: setAsync,
  listKeys: getKeys,
  scanPromise: scan,
  matchKey: matchKey,
  checkRedisCache: checkRedisCache,
  client: client
};
