var fs = require("fs");
var yaml = require("js-yaml");

const QUERIES = "Queries";

function getJSONAttributes(obj) {
  return Object.keys(obj);
}

function capitalizeFirstLetter(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
}

function decapitalizeFirstLetter(string) {
    return string.charAt(0).toLowerCase() + string.slice(1);
}

function removeS(string) {
    let l = string.length - 1;

    if (string.charAt(l) == 's')
      return string.substring(0, l);
    else
      return string;
}

function capitalizeAndRemoveS(string) {
  return removeS(capitalizeFirstLetter(string));
}

function getCacheTable(endPoint) {
    // From GETRestaurants.php to RestaurantsQueries
    return getTableName(endPoint) + QUERIES;
}

function getTableName(endPoint) {
  // From GETRestaurants.php to Restaurants
  return endPoint.substring(3, endPoint.length-4)
}

// YAML Reader and writer
function loadYAML(path) {
  try {
    var f = yaml.safeLoad(fs.readFileSync(path, 'utf8'));
    return f;
  } catch (e) {
    console.log(e);
    return null;
  }
}

function saveYAML(content ,path) {
  try {
     fs.writeFileSync(path, yaml.safeDump(content))
   } catch(err) {
     console.log(err);
   }
}


// Promisifies the HTTP server close function
function closeHTTPServer(server) {
  return new Promise((resolve, reject) => {
    server.close(resolve)
  });
}

module.exports = {
  getJSONAttributes: getJSONAttributes,
  capitalizeFirstLetter: capitalizeFirstLetter,
  decapitalizeFirstLetter: decapitalizeFirstLetter,
  removeS: removeS,
  capitalizeAndRemoveS: capitalizeAndRemoveS,
  loadYAML: loadYAML,
  saveYAML: saveYAML,
  getCacheTable: getCacheTable,
  getTableName: getTableName,
  closeHTTPServer: closeHTTPServer
}
