Select name, address, url, latitude, longitude, cuisines, thumb, rating FROM Restaurants
where DISTANCE_BETWEEN(45.4754429, 9.2239896, latitude, longitude) < 5
ORDER BY DISTANCE_BETWEEN(45.4754429, 9.2239896, latitude, longitude) ASC
