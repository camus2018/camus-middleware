var mysql = require("promise-mysql");
var redis = require("./utils/redis").client;

const dbConfig = {
  host: 'localhost',
  user: 'root',
  password: 'camus2018!',
  database: 'camus',
  connectionLimit: 10
};

var pool = mysql.createPool(dbConfig);

function rawQuery(query) {
  return pool.query(query).then( (res)=> {
    console.log(res);
  }).catch((err) => console.log(err));
}

function deleteAll() {
  rawQuery("DROP TABLE IF EXISTS Restaurants")//.then((res) => console.log(re)).catch((err) => console.log(err));
  rawQuery("DROP TABLE IF EXISTS Events")//.catch((err) => console.log(err));
  rawQuery("DROP TABLE IF EXISTS RestaurantsQueries")//.catch((err) => console.log(err));
  rawQuery("DROP TABLE IF EXISTS EventsQueries")//.catch((err) => console.log(err));
  rawQuery("DROP FUNCTION IF EXISTS DISTANCE_BETWEEN")
  rawQuery("DROP FUNCTION IF EXISTS levenshtein")
  rawQuery("DROP FUNCTION IF EXISTS levenshtein_ratio")
}

function createAll() {
  rawQuery(`CREATE TABLE \`Events\` (
   \`source\` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
   \`query_lat\` double NOT NULL,
   \`query_long\` double NOT NULL,
   \`query_city\` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
   \`query_text\` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
   \`title\` varchar(300) NOT NULL,
   \`venue_name\` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
   \`venue_address\` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
   \`venue_url\` TEXT COLLATE utf8_unicode_ci DEFAULT NULL,
   \`event_url\` TEXT COLLATE utf8_unicode_ci DEFAULT NULL,
   \`all_day\` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
   \`start_time\` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
   \`end_time\` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
   \`latitude\` double DEFAULT NULL,
   \`longitude\` double DEFAULT NULL,
   \`geohash\` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
   \`created_at\` datetime NOT NULL,
   \`updated_at\` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
   \`deleted_at\` datetime NOT NULL,
   PRIMARY KEY (\`source\`,\`title\`,\`venue_name\`)
 ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;`);
  rawQuery(`CREATE TABLE \`Restaurants\` (
   \`source\` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
   \`query_lat\` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
   \`query_long\` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
   \`query_city\` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
   \`query_text\` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
   \`name\` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
   \`address\` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
   \`url\` TEXT COLLATE utf8_unicode_ci DEFAULT NULL,
   \`latitude\` double NOT NULL,
   \`longitude\` double NOT NULL,
   \`geohash\` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
   \`cuisines\` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
   \`thumb\` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
   \`phone\` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
   \`rating\` float DEFAULT NULL,
   \`created_at\` datetime DEFAULT NULL,
   \`updated_at\` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
   \`deleted_at\` datetime DEFAULT NULL,
   PRIMARY KEY (\`source\`,\`name\`,\`address\`,\`latitude\`,\`longitude\`)
 ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci`);
  rawQuery(`CREATE TABLE \`EventsQueries\` (
    \`QueryID\` int(11) NOT NULL AUTO_INCREMENT,
    \`Latitude\` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
    \`Longitude\` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
    \`City\` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
    \`Text\` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
    \`created_at\` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (\`QueryID\`)
  ) ENGINE=InnoDB AUTO_INCREMENT=63 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;`);
  rawQuery(`CREATE TABLE \`RestaurantsQueries\` (
    \`QueryID\` int(11) NOT NULL AUTO_INCREMENT,
    \`Latitude\` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
    \`Longitude\` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
    \`City\` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
    \`Text\` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
    \`created_at\` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (\`QueryID\`)
  ) ENGINE=InnoDB AUTO_INCREMENT=369 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;`);
  rawQuery(`CREATE FUNCTION DISTANCE_BETWEEN (lat1 DOUBLE, lon1 DOUBLE, lat2 DOUBLE, lon2 DOUBLE)
  RETURNS DOUBLE DETERMINISTIC
  RETURN ACOS( SIN(lat1*PI()/180)*SIN(lat2*PI()/180) + COS(lat1*PI()/180)*COS(lat2*PI()/180)*COS(lon2*PI()/180-lon1*PI()/180) ) * 6371;`);
  rawQuery(`
            CREATE FUNCTION levenshtein( s1 VARCHAR(255), s2 VARCHAR(255) )
              RETURNS INT
              DETERMINISTIC
              BEGIN
                DECLARE s1_len, s2_len, i, j, c, c_temp, cost INT;
                DECLARE s1_char CHAR;
                -- max strlen=255
                DECLARE cv0, cv1 VARBINARY(256);
                SET s1_len = CHAR_LENGTH(s1), s2_len = CHAR_LENGTH(s2), cv1 = 0x00, j = 1, i = 1, c = 0;
                IF s1 = s2 THEN
                  RETURN 0;
                ELSEIF s1_len = 0 THEN
                  RETURN s2_len;
                ELSEIF s2_len = 0 THEN
                  RETURN s1_len;
                ELSE
                  WHILE j <= s2_len DO
                    SET cv1 = CONCAT(cv1, UNHEX(HEX(j))), j = j + 1;
                  END WHILE;
                  WHILE i <= s1_len DO
                    SET s1_char = SUBSTRING(s1, i, 1), c = i, cv0 = UNHEX(HEX(i)), j = 1;
                    WHILE j <= s2_len DO
                      SET c = c + 1;
                      IF s1_char = SUBSTRING(s2, j, 1) THEN
                        SET cost = 0; ELSE SET cost = 1;
                      END IF;
                      SET c_temp = CONV(HEX(SUBSTRING(cv1, j, 1)), 16, 10) + cost;
                      IF c > c_temp THEN SET c = c_temp; END IF;
                        SET c_temp = CONV(HEX(SUBSTRING(cv1, j+1, 1)), 16, 10) + 1;
                        IF c > c_temp THEN
                          SET c = c_temp;
                        END IF;
                        SET cv0 = CONCAT(cv0, UNHEX(HEX(c))), j = j + 1;
                    END WHILE;
                    SET cv1 = cv0, i = i + 1;
                  END WHILE;
                END IF;
                RETURN c;
              END;`);
  rawQuery(`
            CREATE FUNCTION \`levenshtein_ratio\`( s1 text, s2 text ) RETURNS int(11)
                DETERMINISTIC
            BEGIN
                DECLARE s1_len, s2_len, max_len INT;
                SET s1_len = LENGTH(s1), s2_len = LENGTH(s2);
                IF s1_len > s2_len THEN
                  SET max_len = s1_len;
                ELSE
                  SET max_len = s2_len;
                END IF;
                RETURN ROUND((1 - LEVENSHTEIN(s1, s2) / max_len) * 100);
              END;
          `);
}

function deleteRedis() {
  redis.flushdb((err, suc) => {console.log(err, suc);})
}

deleteRedis();
deleteAll();
setTimeout(() => {createAll(); console.log('slept');}, 1000);
