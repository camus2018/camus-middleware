var axios = require("axios");
var chalk = require("chalk");

var config = require("./config/config");
var db     = require("./utils/db");
var redis  = require("./utils/redis");
var logger = require("./utils/logger");
var geohash = require("ngeohash");

class Services {

  constructor() {
    this.SQLCACHE = 0;
    this.DIRECTDB = 1;
    this.NOCACHE = 2;
    this.REDISCACHE = 3;
    this.DIRECTENDPOINT = 4;
  }

  // Used only for logging
  _getCacheName(num) {
    switch(num) {
      case 0:
        return 'SQLCACHE';
      case 1:
        return 'DIRECTDB';
      case 2:
        return 'NOCACHE';
      case 3:
        return 'REDISCACHE';
      case 4: 
        return 'DIRECTENDPOINT';
      default:
        return 'UNKNOWN';
    }
  }

  listServices() {
    return axios.get(this.urlServices);
  }
  // In case the stuff is not working
  invokeService(endpoint, params, cacheType) {
    let url = config.backendHost + endpoint + '?' + this.formatUrl(params);
    logger.debug(`Cache ${this._getCacheName(cacheType)} - ${url}`);

    switch(cacheType) {
      case this.NOCACHE:
        return axios.get(url).then((res) => {
        if(res.data.code == 200) {
          return db.executeQuery(endpoint, params);
        } else if (res.data.code == 404){              // Data not found
          logger.warn("404 - No data found for query");
          return null;
        } else {          // Fall back to DB and check results available there
          logger.warn("Fall back to DB to check old results");
          return db.executeQuery(endpoint, params);
        }});
        break;

      case this.DIRECTDB:
          return db.executeQuery(endpoint, params).then((rows) => {
            var limit = params.limit  ||  5;
            if (rows.length < limit) {
              return this.invokeService(endpoint, params, this.NOCACHE);
            } else {
              return rows;
            }
          });


      case this.SQLCACHE:
          return db.checkCache(endpoint, params).then((cacheSize) => {
            logger.debug(`Cache contains ${cacheSize} old queries `);
            if(cacheSize) {
              return this.invokeService(endpoint, params, this.DIRECTDB)
            } else {
              return this.invokeService(endpoint, params, this.NOCACHE)
            }
          });


      case this.REDISCACHE:
          return redis.checkRedisCache(params.query, endpoint, params.lat, params.lon).then((result) => {
              logger.debug(`REDIS Cache returned ${result}`);
              if (result) {
                params.query = result;
                return this.invokeService(endpoint, params, this.DIRECTDB)
              } else {
                return this.invokeService(endpoint, params, this.NOCACHE);
              }
          });

        case this.DIRECTENDPOINT: 
          return axios.get(url).then(res => {
            if (res.status == 200){
              return res.data; 
            } 
            return [];
          });
  
    }
  }

  // Function to create the URL encoded parameters
  formatUrl(params) {
    let url = []
    for (var property in params) {
        if (params.hasOwnProperty(property)) {
          url.push(`${property}=${params[property]}`)
        }
    }
    return url.join('&');
  }
}

module.exports =  Services
