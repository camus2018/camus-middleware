var axios = require("axios")

var config = require("./config/config");

var logger = require("./utils/logger");

function getResourcesSchema() {
  return axios.get(config.resourcesEndpoint).then((res) => {
    return res.data;
  });
}

module.exports = {
  getResourcesSchema: getResourcesSchema
};
