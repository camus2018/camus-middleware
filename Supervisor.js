var bodyParser = require('body-parser');
var express = require('express');
var { ApolloServer } = require('apollo-server-express');

var logger = require("./utils/logger");
var utils = require("./utils/utils");
var config = require("./config/config");

// Controllers import
var { confController } = require("./controllers/confController");

var resolversCreator = require("./resolver");
var schemaBuilder = require('./schemaBuilder');
var validators = require("./core/validators");

const TRACING = true;

class Supervisor {

  constructor(resourcesSchema) {
    logger.info("Creating supervisor...")

    this.resourcesSchema = resourcesSchema;       // Using this resource schema
    this.getConfiguration();
    this.graphql = express();                     // Creating reloadable Express app
    this.graphqlServer;                           // Server created by Apollo Server
    this.graphqlHTTPServer;                       // Store of HTTP Server in order to close it
    // Supervisor Express app
    this.app = express();
    this.app.use(bodyParser.json());
    // CORS Settings
    this.app.use(function(req, res, next) {
        res.header("Access-Control-Allow-Origin", "*");
        res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
        next();
    });
    // End points
    this.app.use('/config', (req, res) => res.send({cdt: this.cdt, mappings: this.mappings, joins: this.joins}))
    this.app.use('/status', confController)
    this.app.use('/resources', (req, res) => res.send(this.resourcesSchema))
    this.app.use('/reload', async (req, res) => {
      this.reload().then((message) => res.send(message));
    });
    this.app.use('/validation', (req,res) => this.validate(this.cdt, this.mappings, this.joins).then((v) => res.send(v)))
    this.app.post('/submit', (req,res) => {

      let promises = []

      const cdt = req.body.cdt || this.cdt;

      // TODO If this fails all other change should be canceled
      if (req.body.cdt) {
          promises.push(validators.validateCDT(cdt, this.resourcesSchema)
          .then(() => {
            utils.saveYAML(cdt, config.CDTPATH)
            return({message: "New CDT saved", cdt: cdt})
          }));
      }

      if (req.body.mappings) {
        const mappings = req.body.mappings      // below may introduce bugs ... to be tested
        promises.push(validators.validateMappings(cdt, mappings, this.resourcesSchema).then((warn) =>{
          utils.saveYAML(mappings, config.MAPPINGSPATH)
          return({message: "New mappings saved", warnings: warn, mappings: mappings})
        }));
      }

      if (req.body.joins) {
        const joins = req.body.joins      // below may introduce bugs ... to be tested
        promises.push(validators.validateJoins(cdt, joins).then((warn) =>{
          utils.saveYAML(joins, config.JOINSPATH)
          return({message: "New joins saved", warnings: warn, joins: joins})
        }));
      }

      const joins = req.body.joins || {};

      Promise.all(promises).then((ret) => {
        console.log(ret)
        res.send({result: ret});
        this.reload();
      }).catch((err) => {
        console.log(err)
        res.send({error: err})
        this.reload();
      })
    });

    // Constructor finishes, load the configuration
    this.reload()

  }

  validate(cdt, mappings, joins) {

    var promises = []

    promises.push(validators.validateCDT(cdt, this.resourcesSchema))
    promises.push(validators.validateMappings(cdt, mappings, this.resourcesSchema))
    promises.push(validators.validateJoins(cdt, joins))

    return Promise.all(promises)

  }

  async reload() {

   if(this.graphqlHTTPServer) {
     await utils.closeHTTPServer(this.graphqlHTTPServer);
   }

   this.graphql = express();

   try {

        await this.getConfiguration();

        const types = schemaBuilder.formatTypes(this.resourcesSchema, this.cdt);
        const resol = resolversCreator.rootResolverCreator(this.cdt, this.resourcesSchema, this.mappings, this.joins);
        // Try to create a new instance following current configuration
         const graphqlServer = new ApolloServer({
              typeDefs: types,  // Schema is built from RS
              resolvers: resol,     // Build the resolvers here, might be better have cdt, etc. as argument
              tracing: TRACING
         });

         await graphqlServer.applyMiddleware({ app: this.graphql })
         this.graphqlHTTPServer = await this.graphql.listen(config.GRAPHQLPORT, () => {
             logger.info(`🚀  Express GraphQL Server Now Running On localhost:${config.GRAPHQLPORT}/graphql`);
         });

         return {message: "Server reloaded"}

    } catch(e) {
      console.error(e)
      logger.error(e)
      return {error: e.message}
    }

 }

// This function loads the configuratio from the files system
 getConfiguration() {

   this.cdt = utils.loadYAML(config.CDTPATH) || {};
   this.mappings = utils.loadYAML(config.MAPPINGSPATH) || {};
   this.joins = utils.loadYAML(config.JOINSPATH) || {};

 }

}

module.exports = Supervisor
