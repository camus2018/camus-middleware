var backendHost       = "backend"//"ec2-54-174-43-60.compute-1.amazonaws.com";
var redisHost       = "redis";
const resourcesEndpoint = "ListServices.php";

const MAPPINGSPATH = __dirname + "/mappings.yaml";
const CDTPATH = __dirname + "/cdt.yaml";
const JOINSPATH = __dirname + "/topics.yaml";
const GRAPHQLPORT    = process.env.GRAPHQLPORT    || 8000;
const SUPERVISORPORT = process.env.SUPERVISORPORT || 8001;

const LOCALHOST = "localhost";

var db = {
  host: 'mysql',
  user: 'root',
  password: 'camus2018!',
  database: 'camus',
  connectionLimit: 10
};

// Switch to make everything run on localhost
if (process.env.CAMUS_ENV == "LOCAL") {
  backendHost = LOCALHOST + ":" + "8082";
  redisHost = LOCALHOST;
  db.host = LOCALHOST;
}

function httpUrl(url) {
  return `http://${url}/`
}

module.exports = {
  backendHost: httpUrl(backendHost),
  redisHost: redisHost,
  resourcesEndpoint: httpUrl(backendHost)+resourcesEndpoint,
  MAPPINGSPATH: MAPPINGSPATH,
  CDTPATH: CDTPATH,
  JOINSPATH: JOINSPATH,
  GRAPHQLPORT: GRAPHQLPORT,
  SUPERVISORPORT: SUPERVISORPORT,
  db: db
}
