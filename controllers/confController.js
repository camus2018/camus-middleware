var config = require("../config/config");

exports.confController = (req, res) => {

  res.json({
    backendHost: config.backendHost,
    redisHost:   config.redisHost,
    resourcesEndpoint: config.resourcesEndpoint,
  });

}
