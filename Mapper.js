var yaml = require("js-yaml");
var fs = require("fs");
var _ = require("lodash");

// var cdtUtils = require("./cdtUtils");
var config = require("./config/config");
var logger = require("./utils/logger");
var utils = require("./utils/utils");

const CDTREGEX = new RegExp(/CDT\.([a-zA-Z-_]*)/); // Regex to find CDT.*
const ANY = "any"; // Attribute holding the any mappings


const getParamsFromMapping = (cdtInstance, mappings, topic ) => {

  var params = {}

  // ANY mappings

  if ( _.has(mappings, [ANY, topic])) {   // If there exists an any mapping for topic
    _.keys(mappings[ANY][topic]).forEach((key) => {  // Check for each ANY topic mapping
          const value = getValue(cdtInstance, mappings[ANY][topic][key])
          if (value) {        // If null is not returned the mapping is added to the params
            params[key] = value
          }
    });
  }

  // Specific mappings

  _.keys(cdtInstance).forEach((dimension) => {
      if(_.has(mappings, [dimension, cdtInstance[dimension], topic])) { // If the mapping for this specific case exists
        const mapVals =  mappings[dimension][cdtInstance[dimension]][topic]
        _.keys(mapVals).forEach((key) => {
          if(getValue(cdtInstance, mapVals[key])) {
              params[key] = getValue(cdtInstance, mapVals[key])
          }
        })
      }
  });

  return params
}


// Check if the topic_parameter value is dynamic or static
// returns the static value or the replaced value from CDT instance if applicable
const getValue = (cdtInstance, value) => {

  const match = value.match(CDTREGEX)

  if (match && match.length > 0) {   // CDT.* found
      if (_.has(cdtInstance, match[1])) {
        return cdtInstance[match[1]]
      } else {
        logger.debug(`Dynamic mapping ${value} with current CDT instance failed`)
        return null
      }
  } else {
    return value // Return value as given as it doesn't contain CDT.*
  }
}


// OLD WAY TO DO IT
// This function takes care of dim:dim_v:topic:topic_param:topic_val
// mappings is the current mappings configuration
// const find = (mappings, cdtInstance, dimension, dimension_v, topic) => {
//
//   logger.debug(`FIND MAP ${dimension} ${dimension_v}`)
//
//   var map = _.clone(mappings[dimension][dimension_v][topic]);
//   logger.debug(map)
//   if (map) {
//     // Loop through the mappings and see if there are some CDT values
//     // i.e mappings that are dynamically set
//     _.keys(map).forEach((key, index) => {
//       var match = map[key].match(CDTREGEX);
//       if (match && match.length > 0) { // Dynamic mapping found
//         map[key] = cdtInstance[match[1]]; // Replace it with value from CDT
//       }
//       // Remove undefined in case they slipped
//       // This should be improved with default values
//       if (!map[key]) {
//         logger.warning(`Dynamic mapping not existant: ${key} Ignoring... `)
//         delete map[key];
//       }
//     });
//
//   } else { // Case mapping not found
//     logger.warning(`Mapping undefined ${dimension} ${dimension_v} ${topic}`);
//   }
//   logger.debug(`MAP: ${dimension} ${topic} <=> ${JSON.stringify(map)}`);
//   return map;
// }



module.exports = {
  getParamsFromMapping: getParamsFromMapping
}
