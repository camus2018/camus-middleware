var config = require("./config/config");
var utils = require("./utils/utils");

var _   = require("lodash");

// Function that format the cdt by separating the dimensions and topics
// Makes the work easier at the level of the creation of resolvers
// Exports an object with the tree, dimensions and topics
// This is only used for the resolvers creation

// The current working cdt is passed as argument to the functions


// Return whatever is under interest_topics
function getCDTTopics(cdt) {
    return cdt.interest_topics;
}

// Return the dimensions (excluding interest_topics)
function getCDTDimensions(cdt) {
  var dimensions = _.remove(_.keys(cdt), (e) => (e != "interest_topics"))
                    .map(e => `${e}: String`);
  return dimensions;
}

// Combines the above functions
function getCDTDetails(cdt) {
  return {
    topics: getCDTTopics(cdt),
    dimensions: getCDTDimensions(cdt)
  }
}

module.exports = {
  getCDTTopics: getCDTTopics,
  getCDTDimensions: getCDTDimensions,
  getCDTDetails: getCDTDetails
};
