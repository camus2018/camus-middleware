
var _ = require('lodash');

  //
  //     Validation promises
  //

  // First validation: CDT vs Resource Schema
  const validation_resourceschema = (cdt, rSchema) => {
    return new Promise((resolve, reject) => {
        if (_.has(cdt, 'interest_topics')) {  // check if it has interest_topics
            const differences = _.difference(cdt.interest_topics, _.keys(rSchema));
            if (!differences.length) {
              resolve([]);
            } else {
              reject(`Some topics exist in CDT but not in Resource Schema : ${differences.join()}`)
            }
        } else {
          reject("No interest topics in CDT")
        }
      })
  }

  // Second validation: CDT vs Mappings
  const validation_mappings = (cdt, mappings, rSchema) => {
    return new Promise((resolve, reject) => {
      let dwarnings = [];
      let dvwarnings = [];
      let topicswarnings = [];
      let topicsparamswarnings = [];
      // Dimensions differences
      const dDifferences = _.difference(_.keys(mappings), _.keys(cdt)).filter((e) => e!= 'any');
      // Dimensions values differences
      var dimensions = _.keys(mappings).filter((e) => e!= 'any');
      // Traversal of each level of the mappings tree
      // Storing each level
      dimensions.forEach((dimension) => {
        // 1st level check : dimensions
        if (!_.has(cdt, dimension)) dwarnings.push(`Dimension [${dimension}] does not exist in CDT`);
        // 2nd level check : dimensions values
        const dvDifference = _.difference(_.keys(mappings[dimension]), cdt[dimension])
        if(dvDifference.length) dvwarnings.push(`Dimension values [${dvDifference}] do not exist in CDT dimension ${dimension}`)
        // 3rd level check : topics (are topics in cdt ?)
        var topicsDifference = []
        _.keys(mappings[dimension]).forEach((dv) => {
          topicsDifference = _.union(topicsDifference, _.difference(_.keys(mappings[dimension][dv]), cdt.interest_topics));
        // 4th level check : Topics param
        _.keys(mappings[dimension][dv]).forEach((topic) => {
          if (!_.includes(topicsDifference, topic)) {    // A check to make sure the topic exists in cdt (passed previous check)
            const paramsDifference = _.difference(_.keys(mappings[dimension][dv][topic]), _.keys(rSchema[topic].params))
            if (paramsDifference.length) topicsparamswarnings.push(`Topic "${topic}" cannot be filtered using [${paramsDifference}]`)
            // TODO add check if * in CDT.* exists in CDT
            }
          })
        });
        if (topicsDifference.length) topicswarnings.push(`Topics [${topicsDifference}] do not exist in CDT interest topics`);
      });
      const warnings = dDifferences.map((e) => `Dimensions [${dDifferences}] do not exist in CDT`); // Array storing the different problems found
      resolve(_.concat(warnings, dvwarnings, topicswarnings, topicsparamswarnings).map(e => ("mappings.yaml: " + e)))
    });
  }
  // Third validation: Joined topics (nested topics )
  const validation_joins = (cdt, joins) => {
    return new Promise((resolve, reject) => {
      let warnings = []
      // Only allowing two level nesting
      _.difference(_.keys(joins), cdt.interest_topics).forEach((e) => warnings.push(`1st level topics [${e}] do not exist in CDT`));
      _.keys(joins).forEach((t) => {
        _.difference(_.keys(joins[t]), cdt.interest_topics).forEach((e) => warnings.push(`2nd level topics [${e}] do not exist in CDT`));
      })
      resolve(warnings)
    });
  }

  module.exports = {

    validateCDT: validation_resourceschema,
    validateMappings: validation_mappings,
    validateJoins: validation_joins,


  }
