var { getCDTDetails } = require("./cdtUtils")
var _   = require("lodash");
var chalk   = require("chalk");
var config = require("./config/config");
var utils = require("./utils/utils");
var logger = require("./utils/logger");
var { getArgsFromInfo } = require("./utils/graphql-utils");

var Services = require("./Services");
var mapper = require("./mapper");

var services = new Services(false);

// Used to get lat/lon from parent and pass it to child query
const LATITUDE = "latitude";
const LONGITUDE = "longitude";

const CACHINGPOLICY = services.DIRECTDB;

logger.info(`CACHING POLICY ${CACHINGPOLICY}`);

function rootResolverCreator(cdt, resourcesSchema, mappings, joins) {

    let rootResolver  = {};
    let joinsResolvers = {};
    // For each topic we need a resolver calling one endpoint on the backend
    for (var i in cdt.interest_topics) {

      var topic = cdt.interest_topics[i]
      var topicEndpoint = resourcesSchema[topic].endpoint
      // Removing the last 's' from topics to comply with GraphQL best practices
      var topicS = topic.substring(0, topic.length - 1)
      // Get the main resolver
      rootResolver[topicS] = getResolver(topic, topicEndpoint, mappings);

      if (_.has(joins, topic)) {

        const ctopic = utils.capitalizeAndRemoveS(topic)


        joinsResolvers[ctopic] = {}

        _.keys(joins[topic]).forEach((t) => {

            const ts = utils.removeS(t)
            joinsResolvers[ctopic][t] = getNestedResolver(ts, resourcesSchema[t].endpoint, mappings, joins[topic][t])

        })

      }

      // Check if topics'  configuration has a nested topic for this topic
      // if(_.has(nestedTopics, topic)) {
      //   var nested = utils.getJSONAttributes(nestedTopics[topic]);
      //   // Initialiaze the object
      //   nestedResolvers[utils.capitalizeAndRemoveS(topic)] = {};
      //   // Add the differents resolvers for each nested object
      //   for (t in nested) {
      //     nestedResolvers[utils.capitalizeAndRemoveS(topic)][nested[t]]= getNestedResolver(nested[t]+'s', 'GETRestaurants.php');
      //   }
      // }
    }

    var resolvers = {
      Query: rootResolver,
       ...joinsResolvers
    }

    return resolvers;
}

function getResolver(topic, endpoint, mappings) {
  return function (root, args) {   // args is the cdt passed in the GraphQuery params
      let cachingPolicy = CACHINGPOLICY;
      // Workaround for newly introduced policy DIRECTENDPOINT (GETNews returns directly in the REST call so we need it)
      // Shoulb be configurable for each entity 
      if (topic == 'news') cachingPolicy = 4; 
      const params = mapper.getParamsFromMapping(args, mappings, topic)
      return services.invokeService(endpoint, params, cachingPolicy);
  }
}

function getNestedResolver(topic, endpoint, mappings, joinsAtts) {
  return function (parent,x,y,info) {

    const args = getArgsFromInfo(info);

    const params = mapper.getParamsFromMapping(args, mappings, `${topic}s`)
    console.log(params)
    console.log(joinsAtts)

    _.keys(joinsAtts).forEach((k) => {

      params[joinsAtts[k]] = parent[k]

    })
     console.log(params)
    // console.log(joinsAtts)
    let cachingPolicy = CACHINGPOLICY;
    // Workaround for newly introduced policy DIRECTENDPOINT (GETNews returns directly in the REST call so we need it)
    // Shoulb be configurable for each entity 
    if (topic == 'new') cachingPolicy = 4; 
    return services.invokeService(endpoint, params, cachingPolicy);
  }
}

module.exports = {
    rootResolverCreator: rootResolverCreator
}
