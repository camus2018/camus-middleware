var _ = require("lodash");

var cdtUtils = require("./cdtUtils");
var utils = require("./utils/utils");
var config = require("./config/config");

var { buildSchema } = require('graphql');

const TYPE = "type";
const QUERY = "Query";
const LBRACE = "{";
const RBRACE = "}";
const LSQBRACE = "[";
const RSQBRACE = "]";

// // GraphQL schema
// var schema = buildSchema(`
//     type Query {
//         restaurant(${formatCDTAttributes()}): [Restaurant]
//     }
//     type Restaurant {
//         name: String
//         address: String
//         url: String
//         latitude: String
//         longitude: String
//         cuisines: String
//         thumb: String
//         rating: String
//         events: Event
//     }
// `);


function getGQLSchema(schema) {
  return buildSchema(formatTypes(schema));
}

function formatTypes(schema, cdt) {

  var cdtDetails = cdtUtils.getCDTDetails(cdt);

  var entities = _.intersection(_.keys(schema), cdtDetails.topics) // Intersect resource schema and cdt;
  var types = [];            // Types in GQL Schema
  var queries = [];          // Queries in GQL Schema

  // !! TODO !! Should not be loaded from fs
  var nestedTopics = utils.loadYAML(config.JOINSPATH);

  for ( i in entities) {
    name = entities[i];

    var attributesName = Object.keys(schema[name].attributes);
    var attributesList = [];
    // Adding the attributes coming from backend server
    for (a in attributesName) {
      var type = schema[name].attributes[attributesName[a]];
      type = utils.capitalizeFirstLetter(type);
      attributesList.push(`${attributesName[a]}: ${type}\n`);
    }
    // Adding nested attributes (Topic inside topic)
    if(nestedTopics && nestedTopics[name]) {

      var topics = utils.getJSONAttributes(nestedTopics[name]);

      for (t in topics) {
        // MODIFY here for list or single in nested entities
        const push =topics[t]

        attributesList.push(`${push}: ${LSQBRACE}${utils.removeS(utils.capitalizeFirstLetter(push))}${RSQBRACE}`)
      }

    }

    // Generating the string
    types.push(`${TYPE} ${utils.capitalizeAndRemoveS(name)} ${LBRACE}
                ${attributesList.join(" ")}
                ${RBRACE}`);

    queries.push(`${utils.removeS(name)}(${formatCDTDimensions(cdtDetails)}): ${LSQBRACE}${utils.capitalizeAndRemoveS(name)}${RSQBRACE} \n`);

    }

    return `${TYPE} ${QUERY} ${LBRACE}
            ${queries.join(" ")}
            ${RBRACE}
            ${types.join(" ")}`;
}


function formatCDTTopics(cdtDetails) {
  return cdtDetails.topics.join(", ");
}

function formatCDTDimensions(cdtDetails) {
  return cdtDetails.dimensions.join(", ");
}

module.exports = {
  getGQLSchema: getGQLSchema,
  formatTypes: formatTypes
};
