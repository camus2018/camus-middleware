const redis = require("./utils/redis")
var geohash = require("ngeohash")

// redis('my test key').then((val) => {
//
//   if(val) {
//     // Check if date is fresh enough
//     console.log(`Value ==> ${val}`)
//   } else {
//     console.log("Null")
//   }
//
// });

// redis.get('Events:::drt2z:::music').then((val) => console.log(val))

redis.listKeys('*').then((keys) => {
  console.log(`${keys.length} keys received ... `)
  console.log(keys);
})

// console.log(geohash.decode("drt2z"));g
// console.log(geohash.encode(42.34130859375,-71.03759765625, 5));

var cursor = '0';
var matches = [];

function scan(){
  redis.client.scan(cursor, 'MATCH', 'R*', 'COUNT', '1', function(err, reply){
    if(err){
        throw err;
    }
    cursor = reply[0];
    console.log(reply);
    if(cursor === '0'){
        matches = matches + reply[1]
        return console.log('Scan Complete');
    }else{
        // console.log("MOOOOOORE")
        matches = matches + reply[1]
        return scan();
    }
  });
}

// scan(); //call scan function

// redis.scanPromise(cursor, 'MATCH', 'R*', 'COUNT', '1').then((val) => {
//   console.log(val)
//   if(reply[0] === '0') {
//     resolve()
//   } e
// })

async function scanAsync(cursor, pattern, results) {
   return redis.scanPromise(cursor, 'MATCH', pattern, 'COUNT', '10')
       .then(function(reply) {

           let keys = reply[1]
           keys.forEach(function(key) {
                console.log(key);
               results.push(key)
           })

           cursor = reply[0]
           if(cursor === '0') {
               console.log('Scan complete')
           } else {
               return scanAsync(cursor, pattern, results)
           }
       })
}



const start = async function(a, b) {
  results = []
  await scanAsync('0', '*', results);
  console.log(results);
}

// start();
// redis.matchKey("R*").then((r) => {console.log(r)});



// redis.checkRedisCache("burger" ,"Restaurants", 52.53662109, 13.38134766)
//      .then((res) => console.log(res))
